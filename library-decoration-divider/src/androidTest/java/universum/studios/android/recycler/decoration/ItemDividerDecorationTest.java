/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.recycler.decoration;

import android.graphics.drawable.ColorDrawable;

import org.junit.Test;

import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class ItemDividerDecorationTest extends AndroidTestCase {

	@SuppressWarnings({"ConstantConditions"})
	@Test public void testInstantiationWithContextAttrsSetDefStyleAttrDefStyleRes() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.ItemDecoration.Divider",
				"style",
				context().getPackageName()
		);
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration(context(), null, 0, styleResId);
		// Assert:
		assertThat(decoration.getOrientation(), is(ItemDividerDecoration.HORIZONTAL));
		assertThat(decoration.getDivider(), instanceOf(ColorDrawable.class));
		assertThat(((ColorDrawable) decoration.getDivider()).getColor(), is(context().getResources().getColor(android.R.color.darker_gray)));
		assertThat(decoration.getDividerThickness(), is(2));
		assertThat(decoration.getDividerOffsetStart(), is(4));
		assertThat(decoration.getDividerOffsetEnd(), is(8));
		assertThat(decoration.getOffsetDrawable(), instanceOf(ColorDrawable.class));
		assertThat(((ColorDrawable) decoration.getOffsetDrawable()).getColor(), is(context().getResources().getColor(android.R.color.white)));
		assertThat(decoration.skipsFirst(), is(true));
		assertThat(decoration.skipsLast(), is(true));
	}

	@Test public void testInstantiationWithContextAttrsSetDefStyleAttrDefStyleResSkipNone() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.ItemDecoration.Divider.SkipNone",
				"style",
				context().getPackageName()
		);
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration(context(), null, 0, styleResId);
		// Assert:
		assertThat(decoration.skipsFirst(), is(false));
		assertThat(decoration.skipsLast(), is(false));
	}

	@Test public void testInstantiationWithContextAttrsSetDefStyleAttrDefStyleResWithSkipFirst() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.ItemDecoration.Divider.SkipFirst",
				"style",
				context().getPackageName()
		);
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration(context(), null, 0, styleResId);
		// Assert:
		assertThat(decoration.skipsFirst(), is(true));
		assertThat(decoration.skipsLast(), is(true));
	}

	@Test public void testInstantiationWithContextAttrsSetDefStyleAttrDefStyleResWithSkipLast() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.ItemDecoration.Divider.SkipLast",
				"style",
				context().getPackageName()
		);
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration(context(), null, 0, styleResId);
		// Assert:
		assertThat(decoration.skipsFirst(), is(false));
		assertThat(decoration.skipsLast(), is(true));
	}

	@Test public void testInstantiationWithContextAttrsSetDefStyleAttrDefStyleResWithSkipBoth() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.ItemDecoration.Divider.SkipBoth",
				"style",
				context().getPackageName()
		);
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration(context(), null, 0, styleResId);
		// Assert:
		assertThat(decoration.skipsFirst(), is(true));
		assertThat(decoration.skipsLast(), is(true));
	}
}