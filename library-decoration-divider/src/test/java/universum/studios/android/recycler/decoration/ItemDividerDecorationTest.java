/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.recycler.decoration;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.TextView;

import org.junit.Test;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.internal.util.MockUtil.resetMock;

/**
 * @author Martin Albedinsky
 */
public final class ItemDividerDecorationTest extends AndroidTestCase {

	private static final int MOCK_ITEMS_COUNT = 10;
	private static final Drawable DIVIDER = new ColorDrawable(Color.TRANSPARENT);

	private final Canvas mockCanvas;
	private final RecyclerView mockRecyclerView;
	private final RecyclerView.State mockRecyclerViewState;
	private View itemView;

	public ItemDividerDecorationTest() {
		this.mockCanvas = mock(Canvas.class);
		this.mockRecyclerView = mock(RecyclerView.class);
		this.mockRecyclerViewState = mock(RecyclerView.State.class);
	}

	@Override public void beforeTest() {
		super.beforeTest();
		this.itemView = new TextView(context());
		resetMock(mockCanvas);
		resetMock(mockRecyclerView);
		when(mockRecyclerView.getWidth()).thenReturn(1080);
		when(mockRecyclerView.getHeight()).thenReturn(1920);
		when(mockRecyclerView.getLayoutManager()).thenReturn(new LinearLayoutManager(context()));
		when(mockRecyclerView.getChildCount()).thenReturn(MOCK_ITEMS_COUNT);
		when(mockRecyclerView.getChildAt(anyInt())).thenReturn(itemView);
		resetMock(mockRecyclerViewState);
		when(mockRecyclerViewState.getItemCount()).thenReturn(MOCK_ITEMS_COUNT);
	}

	@Override public void afterTest() {
		super.afterTest();
		this.itemView = null;
	}

	@Test public void testInstantiation() {
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		// Assert:
		assertThat(decoration.skipsFirst(), is(false));
		assertThat(decoration.skipsLast(), is(true));
		assertThat(decoration.getOrientation(), is(ItemDividerDecoration.VERTICAL));
		assertThat(decoration.getDivider(), is(nullValue()));
		assertThat(decoration.getDividerThickness(), is(0));
		assertThat(decoration.getDividerOffsetStart(), is(0));
		assertThat(decoration.getDividerOffsetEnd(), is(0));
		assertThat(decoration.getPrecondition(), is(RecyclerViewItemDecoration.Precondition.EMPTY));
	}

	@Test public void testInstantiationWithDividerAndVerticalOrientation() {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		when(mockDivider.getIntrinsicWidth()).thenReturn(4);
		when(mockDivider.getIntrinsicHeight()).thenReturn(2);
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration(ItemDividerDecoration.VERTICAL, mockDivider);
		// Assert:
		assertThat(decoration.getOrientation(), is(ItemDividerDecoration.VERTICAL));
		assertThat(decoration.getDivider(), is(mockDivider));
		assertThat(decoration.getDividerThickness(), is(mockDivider.getIntrinsicHeight()));
	}

	@Test public void testInstantiationWithDividerAndHorizontalOrientation() {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		when(mockDivider.getIntrinsicWidth()).thenReturn(4);
		when(mockDivider.getIntrinsicHeight()).thenReturn(2);
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration(ItemDividerDecoration.HORIZONTAL, mockDivider);
		// Assert:
		assertThat(decoration.getOrientation(), is(ItemDividerDecoration.HORIZONTAL));
		assertThat(decoration.getDivider(), is(mockDivider));
		assertThat(decoration.getDividerThickness(), is(mockDivider.getIntrinsicWidth()));
	}

	@Test public void testInstantiationWithContext() {
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration(context());
		// Assert:
		assertThat(decoration.skipsFirst(), is(false));
		assertThat(decoration.skipsLast(), is(true));
	}

	@Test public void testInstantiationWithContextAttrsSet() {
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration(context(), null);
		// Assert:
		assertThat(decoration.skipsFirst(), is(false));
		assertThat(decoration.skipsLast(), is(true));
	}

	@Test public void testInstantiationWithContextAttrsSetDefStyleAttr() {
		// Act:
		final ItemDividerDecoration decoration = new ItemDividerDecoration(context(), null, 0);
		// Assert:
		assertThat(decoration.skipsFirst(), is(false));
		assertThat(decoration.skipsLast(), is(true));
	}

	@Test public void testOrientation() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		// Act + Assert:
		decoration.setOrientation(ItemDividerDecoration.HORIZONTAL);
		assertThat(decoration.getOrientation(), is(ItemDividerDecoration.HORIZONTAL));
		decoration.setOrientation(ItemDividerDecoration.VERTICAL);
		assertThat(decoration.getOrientation(), is(ItemDividerDecoration.VERTICAL));
	}

	@Test public void testSetOrientationAfterDivider() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final Drawable mockDivider = mock(Drawable.class);
		when(mockDivider.getIntrinsicWidth()).thenReturn(4);
		when(mockDivider.getIntrinsicHeight()).thenReturn(2);
		decoration.setDivider(mockDivider);
		// Act + Assert:
		decoration.setOrientation(ItemDividerDecoration.HORIZONTAL);
		assertThat(decoration.getDividerThickness(), is(mockDivider.getIntrinsicWidth()));
		decoration.setOrientation(ItemDividerDecoration.VERTICAL);
		assertThat(decoration.getDividerThickness(), is(mockDivider.getIntrinsicHeight()));
	}

	@Test public void testSetDividerForVerticalOrientation() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setOrientation(ItemDividerDecoration.VERTICAL);
		final Drawable mockDivider = mock(Drawable.class);
		when(mockDivider.getIntrinsicWidth()).thenReturn(4);
		when(mockDivider.getIntrinsicHeight()).thenReturn(2);
		// Act:
		decoration.setDivider(mockDivider);
		// Assert:
		assertThat(decoration.getDividerThickness(), is(mockDivider.getIntrinsicHeight()));
	}

	@Test public void testSetDividerForHorizontalOrientation() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setOrientation(ItemDividerDecoration.HORIZONTAL);
		final Drawable mockDivider = mock(Drawable.class);
		when(mockDivider.getIntrinsicWidth()).thenReturn(4);
		when(mockDivider.getIntrinsicHeight()).thenReturn(2);
		// Act:
		decoration.setDivider(mockDivider);
		// Assert:
		assertThat(decoration.getDividerThickness(), is(mockDivider.getIntrinsicWidth()));
	}

	@Test public void testSetDividerAfterThickness() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDividerThickness(4);
		final Drawable mockDivider = mock(Drawable.class);
		when(mockDivider.getIntrinsicHeight()).thenReturn(2);
		// Act:
		decoration.setDivider(mockDivider);
		// Assert:
		assertThat(decoration.getDividerThickness(), is(mockDivider.getIntrinsicHeight()));
	}

	@Test public void testDividerThickness() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		// Act + Assert:
		decoration.setDividerThickness(10);
		assertThat(decoration.getDividerThickness(), is(10));
	}

	@Test public void testSetDividerThicknessAfterDivider() {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		when(mockDivider.getIntrinsicHeight()).thenReturn(2);
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(mockDivider);
		// Act:
		decoration.setDividerThickness(4);
		// Assert:
		assertThat(decoration.getDividerThickness(), is(4));
	}

	@Test public void testDividerOffsets() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		// Act + Assert:
		decoration.setDividerOffset(10, 5);
		assertThat(decoration.getDividerOffsetStart(), is(10));
		assertThat(decoration.getDividerOffsetEnd(), is(5));
		decoration.setDividerOffset(1, 2);
		assertThat(decoration.getDividerOffsetStart(), is(1));
		assertThat(decoration.getDividerOffsetEnd(), is(2));
	}

	@Test public void testGetItemOffsetsForVerticalOrientation() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(true);
		decoration.setPrecondition(mockPrecondition);
		decoration.setOrientation(ItemDividerDecoration.VERTICAL);
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(4);
		decoration.setDividerOffset(10, 5);
		// Act:
		final Rect rect = new Rect();
		decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		assertThat(rect.left, is(0));
		assertThat(rect.right, is(0));
		assertThat(rect.top, is(0));
		assertThat(rect.bottom, is(decoration.getDividerThickness()));
		verify(mockRecyclerView).getLayoutDirection();
		verify(mockPrecondition).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
	}

	@Test public void testGetItemOffsetsForVerticalOrientationAndRTLLayoutDirection() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(true);
		decoration.setPrecondition(mockPrecondition);
		decoration.setOrientation(ItemDividerDecoration.VERTICAL);
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(4);
		decoration.setDividerOffset(10, 5);
		when(mockRecyclerView.getLayoutDirection()).thenReturn(ViewCompat.LAYOUT_DIRECTION_RTL);
		// Act:
		final Rect rect = new Rect();
		decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		assertThat(rect.left, is(0));
		assertThat(rect.right, is(0));
		assertThat(rect.top, is(0));
		assertThat(rect.bottom, is(decoration.getDividerThickness()));
		verify(mockRecyclerView).getLayoutDirection();
		verify(mockPrecondition).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
	}

	@Test public void testGetItemOffsetsForHorizontalOrientation() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(true);
		decoration.setPrecondition(mockPrecondition);
		decoration.setOrientation(ItemDividerDecoration.HORIZONTAL);
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(4);
		decoration.setDividerOffset(10, 5);
		// Act:
		final Rect rect = new Rect();
		decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		assertThat(rect.left, is(0));
		assertThat(rect.right, is(decoration.getDividerThickness()));
		assertThat(rect.top, is(0));
		assertThat(rect.bottom, is(0));
		verify(mockPrecondition).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
	}

	@Test public void testGetItemOffsetsSkipFirst() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(true);
		decoration.setPrecondition(mockPrecondition);
		decoration.setSkipFirst(true);
		decoration.setSkipLast(false);
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(4);
		// Act + Assert:
		final Rect rect = new Rect();
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
			decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
			assertThat(rect.left, is(0));
			assertThat(rect.right, is(0));
			assertThat(rect.top, is(0));
			assertThat(rect.bottom, is(i == 0 ? 0 : decoration.getDividerThickness()));
		}
		verify(mockPrecondition, times(itemCount - 1)).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
	}

	@Test public void testGetItemOffsetsSkipLast() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(true);
		decoration.setPrecondition(mockPrecondition);
		decoration.setSkipFirst(false);
		decoration.setSkipLast(true);
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(4);
		// Act + Assert:
		final Rect rect = new Rect();
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
			decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
			assertThat(rect.left, is(0));
			assertThat(rect.right, is(0));
			assertThat(rect.top, is(0));
			assertThat(rect.bottom, is(i == itemCount - 1 ? 0 : decoration.getDividerThickness()));
		}
		verify(mockPrecondition, times(itemCount - 1)).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
	}

	@Test public void testGetItemOffsetsSkipBoth() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(true);
		decoration.setPrecondition(mockPrecondition);
		decoration.setSkipFirst(true);
		decoration.setSkipLast(true);
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(4);
		// Act + Assert:
		final Rect rect = new Rect();
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
			decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
			assertThat(rect.left, is(0));
			assertThat(rect.right, is(0));
			assertThat(rect.top, is(0));
			assertThat(rect.bottom, is(i == 0 || i == itemCount - 1 ? 0 : decoration.getDividerThickness()));
		}
		verify(mockPrecondition, times(itemCount - 2)).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
	}

	@Test public void testGetItemOffsetsWithUnsatisfiedPrecondition() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(false);
		decoration.setPrecondition(mockPrecondition);
		decoration.setOrientation(ItemDividerDecoration.HORIZONTAL);
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(4);
		// Act:
		final Rect rect = new Rect();
		decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		assertThat(rect.isEmpty(), is(true));
		verify(mockPrecondition).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
	}

	@Test public void testGetItemOffsetsForZeroDividerThickness() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(true);
		decoration.setPrecondition(mockPrecondition);
		decoration.setDividerThickness(0);
		final Rect rect = new Rect();
		decoration.setOrientation(ItemDividerDecoration.HORIZONTAL);
		// Act + Assert:
		decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
		assertThat(rect.isEmpty(), is(true));
		decoration.setOrientation(ItemDividerDecoration.VERTICAL);
		decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
		assertThat(rect.isEmpty(), is(true));
		verify(mockRecyclerViewState, times(2)).getItemCount();
		verify(mockRecyclerView, times(2)).getLayoutManager();
		verifyNoMoreInteractions(mockRecyclerView, mockRecyclerViewState);
		verifyZeroInteractions(mockPrecondition);
	}

	@Test public void testGetItemOffsetsForUnknownAdapterPosition() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(true);
		decoration.setPrecondition(mockPrecondition);
		decoration.setSkipFirst(true);
		decoration.setSkipLast(true);
		decoration.setDividerThickness(4);
		final Rect rect = new Rect();
		when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(RecyclerView.NO_POSITION);
		// Act:
		decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		assertThat(rect.isEmpty(), is(true));
		verify(mockRecyclerViewState).getItemCount();
		verifyNoMoreInteractions(mockRecyclerViewState);
		verifyZeroInteractions(mockPrecondition);
	}

	@Test public void testGetItemOffsetsWhenNotVisible() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(false);
		decoration.setPrecondition(mockPrecondition);
		decoration.setVisible(false);
		// Act:
		final Rect rect = new Rect();
		decoration.getItemOffsets(rect, itemView, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		assertThat(rect.isEmpty(), is(true));
		verifyZeroInteractions(mockPrecondition);
	}

	@Test public void testShouldDecorate() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(2);
		// Act + Assert:
		assertThat(decoration.shouldDecorate(mockRecyclerView, mockRecyclerViewState), is(true));
	}

	@Test public void testShouldDecorateWithoutDivider() {
		// Arrange:
		when(mockRecyclerView.getLayoutManager()).thenReturn(new LinearLayoutManager(context()));
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(null);
		decoration.setDividerThickness(2);
		// Act + Assert:
		assertThat(decoration.shouldDecorate(mockRecyclerView, mockRecyclerViewState), is(false));
	}

	@Test public void testShouldDecorateWithZeroDividerThickness() {
		// Arrange:
		when(mockRecyclerView.getLayoutManager()).thenReturn(new LinearLayoutManager(context()));
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(0);
		// Act + Assert:
		assertThat(decoration.shouldDecorate(mockRecyclerView, mockRecyclerViewState), is(false));
	}

	@Test public void testShouldDecorateWithoutDividerAndThicknessAndValidRecyclerViewAndState() {
		// Arrange:
		when(mockRecyclerView.getLayoutManager()).thenReturn(null);
		when(mockRecyclerViewState.getItemCount()).thenReturn(0);
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(null);
		decoration.setDividerThickness(0);
		// Act + Assert:
		assertThat(decoration.shouldDecorate(mockRecyclerView, mockRecyclerViewState), is(false));
	}

	@Test public void testOnDrawForVerticalOrientation() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(true);
		decoration.setPrecondition(mockPrecondition);
		decoration.setOrientation(ItemDividerDecoration.VERTICAL);
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(4);
		// Act:
		decoration.onDraw(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockPrecondition, times(MOCK_ITEMS_COUNT)).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
	}

	@Test public void testOnDrawForHorizontalOrientation() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(true);
		decoration.setPrecondition(mockPrecondition);
		decoration.setOrientation(ItemDividerDecoration.HORIZONTAL);
		decoration.setDivider(DIVIDER);
		decoration.setDividerThickness(4);
		// Act:
		decoration.onDraw(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockPrecondition, times(MOCK_ITEMS_COUNT)).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
	}

	@Test public void testOnDrawForRecyclerViewWithoutLayoutManager() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setOrientation(ItemDividerDecoration.HORIZONTAL);
		when(mockRecyclerView.getLayoutManager()).thenReturn(null);
		// Act:
		decoration.onDraw(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verifyZeroInteractions(mockCanvas);
	}

	@Test public void testOnDrawHorizontally() {
		this.testOnDrawHorizontallyInner(false, false);
	}

	@Test public void testOnDrawHorizontallyForRTLLayoutDirection() {
		when(mockRecyclerView.getLayoutDirection()).thenReturn(ViewCompat.LAYOUT_DIRECTION_RTL);
		this.testOnDrawHorizontallyInner(false, false);
	}

	@Test public void testOnDrawHorizontallySkipFirst() {
		this.testOnDrawHorizontallyInner(true, false);
	}

	@Test public void testOnDrawHorizontallySkipLast() {
		this.testOnDrawHorizontallyInner(false, true);
	}

	@Test public void testOnDrawHorizontallySkipBoth() {
		this.testOnDrawHorizontallyInner(true, true);
	}

	@Test public void testOnDrawHorizontallyWithUnsatisfiedPrecondition() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(false);
		decoration.setPrecondition(mockPrecondition);
		decoration.setSkipLast(false);
		final Drawable mockDivider = mock(Drawable.class);
		decoration.setDivider(mockDivider);
		// Act:
		decoration.onDrawHorizontally(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockPrecondition, times(MOCK_ITEMS_COUNT)).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
		verify(mockDivider, times(0)).draw(mockCanvas);
		verify(mockCanvas).restore();
	}

	private void testOnDrawHorizontallyInner(boolean skipFirst, boolean skipLast) {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setSkipFirst(skipFirst);
		decoration.setSkipLast(skipLast);
		decoration.setDivider(mockDivider);
		decoration.setDividerThickness(4);
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			final View itemView = new View(context());
			when(mockRecyclerView.getChildAt(i)).thenReturn(itemView);
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
		}
		// Act:
		decoration.onDrawHorizontally(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockCanvas, times(0)).clipRect(anyInt(), anyInt(), anyInt(), anyInt());
		verify(mockRecyclerView).getChildCount();
		int verifyTimes = mockRecyclerView.getChildCount();
		verifyTimes -= skipFirst ? 1 : 0;
		verifyTimes -= skipLast ? 1 : 0;
		verify(mockDivider, times(verifyTimes)).setBounds(
				-decoration.getDividerThickness(),
				0,
				0,
				mockRecyclerView.getHeight()
		);
		verify(mockDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockCanvas).restore();
	}

	@Test public void testOnDrawHorizontallyWithOffset() {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		final Drawable mockOffsetDivider = mock(Drawable.class);
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(mockDivider);
		decoration.setDividerThickness(4);
		decoration.setOffset(8, 16);
		decoration.setOffsetDrawable(mockOffsetDivider);
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			final View itemView = new View(context());
			when(mockRecyclerView.getChildAt(i)).thenReturn(itemView);
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
		}
		// Act:
		decoration.onDrawHorizontally(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockRecyclerView).getChildCount();
		int verifyTimes = mockRecyclerView.getChildCount() -1;
		verify(mockOffsetDivider, times(verifyTimes)).setBounds(
				-decoration.getDividerThickness(),
				0,
				0,
				decoration.getOffsetStart()
		);
		verify(mockOffsetDivider, times(verifyTimes)).setBounds(
				-decoration.getDividerThickness(),
				mockRecyclerView.getHeight() - decoration.getOffsetEnd(),
				0,
				mockRecyclerView.getHeight()
		);
		verify(mockOffsetDivider, times(verifyTimes * 2)).draw(mockCanvas);
		verify(mockDivider, times(verifyTimes)).setBounds(
				-decoration.getDividerThickness(),
				decoration.getOffsetStart(),
				0,
				mockRecyclerView.getHeight() - decoration.getOffsetEnd()
		);
		verify(mockDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockCanvas).restore();
	}

	@Test public void testOnDrawHorizontallyWithOffsetStart() {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		final Drawable mockOffsetDivider = mock(Drawable.class);
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(mockDivider);
		decoration.setDividerThickness(4);
		decoration.setOffset(8, 0);
		decoration.setOffsetDrawable(mockOffsetDivider);
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			final View itemView = new View(context());
			when(mockRecyclerView.getChildAt(i)).thenReturn(itemView);
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
		}
		// Act:
		decoration.onDrawHorizontally(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockRecyclerView).getChildCount();
		int verifyTimes = mockRecyclerView.getChildCount() -1;
		verify(mockOffsetDivider, times(verifyTimes)).setBounds(
				-decoration.getDividerThickness(),
				0,
				0,
				decoration.getOffsetStart()
		);
		verify(mockOffsetDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockDivider, times(verifyTimes)).setBounds(
				-decoration.getDividerThickness(),
				decoration.getOffsetStart(),
				0,
				mockRecyclerView.getHeight()
		);
		verify(mockDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockCanvas).restore();
	}

	@Test public void testOnDrawHorizontallyWithOffsetEnd() {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		final Drawable mockOffsetDivider = mock(Drawable.class);
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(mockDivider);
		decoration.setDividerThickness(4);
		decoration.setOffset(0, 16);
		decoration.setOffsetDrawable(mockOffsetDivider);
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			final View itemView = new View(context());
			when(mockRecyclerView.getChildAt(i)).thenReturn(itemView);
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
		}
		// Act:
		decoration.onDrawHorizontally(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockRecyclerView).getChildCount();
		int verifyTimes = mockRecyclerView.getChildCount() -1;
		verify(mockOffsetDivider, times(verifyTimes)).setBounds(
				-decoration.getDividerThickness(),
				mockRecyclerView.getHeight() - decoration.getOffsetEnd(),
				0,
				mockRecyclerView.getHeight()
		);
		verify(mockOffsetDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockDivider, times(verifyTimes)).setBounds(
				-decoration.getDividerThickness(),
				0,
				0,
				mockRecyclerView.getHeight() - decoration.getOffsetEnd()
		);
		verify(mockDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockCanvas).restore();
	}

	@SuppressLint("NewApi")
	@Test public void testOnDrawHorizontallyClipped() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(mock(Drawable.class));
		when(mockRecyclerView.getClipToPadding()).thenReturn(true);
		when(mockRecyclerView.getPaddingLeft()).thenReturn(16);
		when(mockRecyclerView.getPaddingRight()).thenReturn(16);
		when(mockRecyclerView.getPaddingTop()).thenReturn(8);
		when(mockRecyclerView.getPaddingBottom()).thenReturn(8);
		// Act:
		decoration.onDrawHorizontally(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockCanvas).clipRect(
				mockRecyclerView.getPaddingLeft(),
				mockRecyclerView.getPaddingTop(),
				mockRecyclerView.getWidth() - mockRecyclerView.getPaddingRight(),
				mockRecyclerView.getHeight() - mockRecyclerView.getPaddingBottom()
		);
		verify(mockCanvas).restore();
	}

	@Test public void testOnDrawVertically() {
		this.testOnDrawVerticallyInner(false, false);
	}

	@Test public void testOnDrawVerticallyForRTLLayoutDirection() {
		when(mockRecyclerView.getLayoutDirection()).thenReturn(ViewCompat.LAYOUT_DIRECTION_RTL);
		this.testOnDrawVerticallyInner(false, false);
	}

	@Test public void testOnDrawVerticallySkipFirst() {
		this.testOnDrawVerticallyInner(true, false);
	}

	@Test public void testOnDrawVerticallySkipLast() {
		this.testOnDrawVerticallyInner(false, true);
	}

	@Test public void testOnDrawVerticallySkipBoth() {
		this.testOnDrawVerticallyInner(true, true);
	}

	@Test public void testOnDrawVerticallyWithUnsatisfiedPrecondition() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		final ItemDividerDecoration.Precondition mockPrecondition = mock(ItemDividerDecoration.Precondition.class);
		when(mockPrecondition.check(itemView, mockRecyclerView, mockRecyclerViewState)).thenReturn(false);
		decoration.setPrecondition(mockPrecondition);
		decoration.setSkipLast(false);
		final Drawable mockDivider = mock(Drawable.class);
		decoration.setDivider(mockDivider);
		// Act:
		decoration.onDrawVertically(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockPrecondition, times(MOCK_ITEMS_COUNT)).check(itemView, mockRecyclerView, mockRecyclerViewState);
		verifyNoMoreInteractions(mockPrecondition);
		verify(mockDivider, times(0)).draw(mockCanvas);
		verify(mockCanvas).restore();
	}

	private void testOnDrawVerticallyInner(boolean skipFirst, boolean skipLast) {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setSkipFirst(skipFirst);
		decoration.setSkipLast(skipLast);
		decoration.setDivider(mockDivider);
		decoration.setDividerThickness(4);
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			final View itemView = new View(context());
			when(mockRecyclerView.getChildAt(i)).thenReturn(itemView);
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
		}
		// Act:
		decoration.onDrawVertically(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockCanvas, times(0)).clipRect(anyInt(), anyInt(), anyInt(), anyInt());
		verify(mockRecyclerView).getChildCount();
		int verifyTimes = mockRecyclerView.getChildCount();
		verifyTimes -= skipFirst ? 1 : 0;
		verifyTimes -= skipLast ? 1 : 0;
		verify(mockDivider, times(verifyTimes)).setBounds(
				0,
				-decoration.getDividerThickness(),
				mockRecyclerView.getWidth(),
				0
		);
		verify(mockDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockCanvas).restore();
	}

	@SuppressLint("NewApi")
	@Test public void testOnDrawVerticallyClipped() {
		// Arrange:
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(mock(Drawable.class));
		when(mockRecyclerView.getClipToPadding()).thenReturn(true);
		when(mockRecyclerView.getPaddingLeft()).thenReturn(16);
		when(mockRecyclerView.getPaddingRight()).thenReturn(16);
		when(mockRecyclerView.getPaddingTop()).thenReturn(8);
		when(mockRecyclerView.getPaddingBottom()).thenReturn(8);
		// Act:
		decoration.onDrawVertically(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockCanvas).clipRect(
				mockRecyclerView.getPaddingLeft(),
				mockRecyclerView.getPaddingTop(),
				mockRecyclerView.getWidth() - mockRecyclerView.getPaddingRight(),
				mockRecyclerView.getHeight() - mockRecyclerView.getPaddingBottom()
		);
		verify(mockCanvas).restore();
	}

	@Test public void setOnDrawVerticallyWithOffset() {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		final Drawable mockOffsetDivider = mock(Drawable.class);
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(mockDivider);
		decoration.setDividerThickness(4);
		decoration.setOffset(8, 16);
		decoration.setOffsetDrawable(mockOffsetDivider);
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			final View itemView = new View(context());
			when(mockRecyclerView.getChildAt(i)).thenReturn(itemView);
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
		}
		// Act:
		decoration.onDrawVertically(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockRecyclerView).getChildCount();
		final int verifyTimes = mockRecyclerView.getChildCount() - 1;
		verify(mockOffsetDivider, times(verifyTimes)).setBounds(
				0,
				-decoration.getDividerThickness(),
				decoration.getOffsetStart(),
				0
		);
		verify(mockOffsetDivider, times(verifyTimes)).setBounds(
				mockRecyclerView.getWidth() - decoration.getOffsetEnd(),
				-decoration.getDividerThickness(),
				mockRecyclerView.getWidth(),
				0
		);
		verify(mockOffsetDivider, times(verifyTimes * 2)).draw(mockCanvas);
		verify(mockDivider, times(verifyTimes)).setBounds(
				decoration.getOffsetStart(),
				-decoration.getDividerThickness(),
				mockRecyclerView.getWidth() - decoration.getOffsetEnd(),
				0
		);
		verify(mockDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockCanvas).restore();
	}

	@Test public void setOnDrawVerticallyWithOffsetStart() {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		final Drawable mockOffsetDivider = mock(Drawable.class);
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(mockDivider);
		decoration.setDividerThickness(4);
		decoration.setOffset(8, 0);
		decoration.setOffsetDrawable(mockOffsetDivider);
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			final View itemView = new View(context());
			when(mockRecyclerView.getChildAt(i)).thenReturn(itemView);
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
		}
		// Act:
		decoration.onDrawVertically(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockRecyclerView).getChildCount();
		final int verifyTimes = mockRecyclerView.getChildCount() - 1;
		verify(mockOffsetDivider, times(verifyTimes)).setBounds(
				0,
				-decoration.getDividerThickness(),
				decoration.getOffsetStart(),
				0
		);
		verify(mockOffsetDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockDivider, times(verifyTimes)).setBounds(
				decoration.getOffsetStart(),
				-decoration.getDividerThickness(),
				mockRecyclerView.getWidth(),
				0
		);
		verify(mockDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockCanvas).restore();
	}

	@Test public void setOnDrawVerticallyWithOffsetEnd() {
		// Arrange:
		final Drawable mockDivider = mock(Drawable.class);
		final Drawable mockOffsetDivider = mock(Drawable.class);
		final ItemDividerDecoration decoration = new ItemDividerDecoration();
		decoration.setDivider(mockDivider);
		decoration.setDividerThickness(4);
		decoration.setOffset(0, 16);
		decoration.setOffsetDrawable(mockOffsetDivider);
		final int itemCount = mockRecyclerViewState.getItemCount();
		for (int i = 0; i < itemCount; i++) {
			final View itemView = new View(context());
			when(mockRecyclerView.getChildAt(i)).thenReturn(itemView);
			when(mockRecyclerView.getChildAdapterPosition(itemView)).thenReturn(i);
		}
		// Act:
		decoration.onDrawVertically(mockCanvas, mockRecyclerView, mockRecyclerViewState);
		// Assert:
		verify(mockCanvas).save();
		verify(mockRecyclerView).getChildCount();
		final int verifyTimes = mockRecyclerView.getChildCount() - 1;
		verify(mockOffsetDivider, times(verifyTimes)).setBounds(
				mockRecyclerView.getWidth() - decoration.getOffsetEnd(),
				-decoration.getDividerThickness(),
				mockRecyclerView.getWidth(),
				0
		);
		verify(mockOffsetDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockDivider, times(verifyTimes)).setBounds(
				0,
				-decoration.getDividerThickness(),
				mockRecyclerView.getWidth() - decoration.getOffsetEnd(),
				0
		);
		verify(mockDivider, times(verifyTimes)).draw(mockCanvas);
		verify(mockCanvas).restore();
	}
}