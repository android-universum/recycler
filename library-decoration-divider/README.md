Recycler-Decoration-Divider
===============

This module contains **decoration** which draws **divider** for each of items displayed by a 
`RecyclerView` widget.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Arecycler/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Arecycler/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:recycler-decoration-divider:${DESIRED_VERSION}@aar"

_depends on:_
[recycler-decoration-core](https://bitbucket.org/android-universum/recycler/src/main/library-decoration-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [ItemDividerDecoration](https://bitbucket.org/android-universum/recycler/src/main/library-decoration-divider/src/main/java/universum/studios/android/recycler/decoration/ItemDividerDecoration.java)