@Recycler-Decoration
===============

This module groups the following modules into one **single group**:

- [Decoration-Core](https://bitbucket.org/android-universum/recycler/src/main/library-decoration-core)
- [Decoration-Divider](https://bitbucket.org/android-universum/recycler/src/main/library-decoration-divider)
- [Decoration-Space](https://bitbucket.org/android-universum/recycler/src/main/library-decoration-space)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Arecycler/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Arecycler/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:recycler-decoration:${DESIRED_VERSION}@aar"
