Recycler-Helper
===============

This module groups the following modules into one **single group**:

- [Helper-Core](https://bitbucket.org/android-universum/recycler/src/main/library-helper-core)
- [Helper-Drag](https://bitbucket.org/android-universum/recycler/src/main/library-helper-drag)
- [Helper-Swipe](https://bitbucket.org/android-universum/recycler/src/main/library-helper-swipe)

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Arecycler/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Arecycler/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:recycler-helper:${DESIRED_VERSION}@aar"
