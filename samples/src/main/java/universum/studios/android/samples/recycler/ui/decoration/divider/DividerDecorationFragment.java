/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.samples.recycler.ui.decoration.divider;

import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindDimen;
import butterknife.BindDrawable;
import universum.studios.android.recycler.decoration.ItemDividerDecoration;
import universum.studios.android.samples.recycler.R;
import universum.studios.android.samples.recycler.ui.decoration.DecorationSampleFragment;

/**
 * @author Martin Albedinsky
 */
public class DividerDecorationFragment extends DecorationSampleFragment {

	@BindDrawable(R.drawable.divider) Drawable divider;
	@BindDimen(R.dimen.divider_thickness) int dividerThickness;

	@Override public void onCreate(@Nullable final Bundle savedInstanceState) {
		requestFeature(FEATURE_DEPENDENCIES_INJECTION);
		super.onCreate(savedInstanceState);
	}

	@Override protected void onChangeLayoutManager(@NonNull final RecyclerView recyclerView, @NonNull final RecyclerView.LayoutManager layoutManager) {
		super.onChangeLayoutManager(recyclerView, layoutManager);
		if (layoutManager instanceof GridLayoutManager) {
			setItemDecoration(null);
		} else if (layoutManager instanceof LinearLayoutManager) {
			final Resources resources = getResources();
			final ItemDividerDecoration decoration = new ItemDividerDecoration(
					((LinearLayoutManager) layoutManager).getOrientation(),
					divider
			);
			decoration.setThickness(dividerThickness);
			decoration.setOffset(
					resources.getDimensionPixelSize(R.dimen.ui_spacing_secondary),
					resources.getDimensionPixelSize(R.dimen.ui_spacing_secondary)
			);
			decoration.setOffsetDrawable(new ColorDrawable(Color.TRANSPARENT));
			setItemDecoration(decoration);
		} else {
			setItemDecoration(null);
		}
	}
}