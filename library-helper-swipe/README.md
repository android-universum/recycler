Recycler-Helper-Swipe
===============

This module contains **helper** which may be used to handle **swipe** gesture for items displayed by
a `RecyclerView` widget.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Arecycler/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Arecycler/_latestVersion)

### Gradle ###

    implementation "universum.studios.android:recycler-helper-swipe:${DESIRED_VERSION}@aar"

_depends on:_
[recycler-helper-core](https://bitbucket.org/android-universum/recycler/src/main/library-helper-core)

## Elements ##

Below are listed some of **primary elements** that are available in this module:

- [ItemSwipeHelper](https://bitbucket.org/android-universum/recycler/src/main/library-helper-swipe/src/main/java/universum/studios/android/recycler/helper/ItemSwipeHelper.java)
