Android Recycler
===============

[![CircleCI](https://circleci.com/bb/android-universum/recycler.svg?style=shield)](https://circleci.com/bb/android-universum/recycler)
[![Codecov](https://codecov.io/bb/android-universum/recycler/branch/main/graph/badge.svg)](https://codecov.io/bb/android-universum/recycler)
[![Codacy](https://api.codacy.com/project/badge/Grade/cd4ef1bf15184a66bf60a40ed7984b64)](https://www.codacy.com/app/universum-studios/recycler?utm_source=android-universum@bitbucket.org&amp;utm_medium=referral&amp;utm_content=android-universum/recycler&amp;utm_campaign=Badge_Grade)
[![Android](https://img.shields.io/badge/android-9.0-blue.svg)](https://developer.android.com/about/versions/pie/android-9.0)
[![Robolectric](https://img.shields.io/badge/robolectric-4.3.1-blue.svg)](http://robolectric.org)
[![Android Jetpack](https://img.shields.io/badge/Android-Jetpack-brightgreen.svg)](https://developer.android.com/jetpack)

Common RecyclerView decorations and helpers for the Android platform.

For more information please visit the **[Wiki](https://bitbucket.org/android-universum/recycler/wiki)**.

## Download ##
[![Bintray](https://api.bintray.com/packages/universum-studios/android/universum.studios.android%3Arecycler/images/download.svg)](https://bintray.com/universum-studios/android/universum.studios.android%3Arecycler/_latestVersion)

Download the latest **[release](https://bitbucket.org/android-universum/recycler/addon/pipelines/deployments "Deployments page")** or **add as dependency** in your project via:

### Gradle ###

    implementation "universum.studios.android:recycler:${DESIRED_VERSION}@aar"

## Modules ##

This library may be used via **separate [modules](https://bitbucket.org/android-universum/recycler/src/main/MODULES.md)**
in order to depend only on desired _parts of the library's code base_ what ultimately results in **fewer dependencies**.

## Compatibility ##

Supported down to the **Android [API Level 14](http://developer.android.com/about/versions/android-4.0.html "See API highlights")**.

### Dependencies ###

- [`androidx.annotation:annotation`](https://developer.android.com/jetpack/androidx)
- [`androidx.interpolator:interpolator`](https://developer.android.com/jetpack/androidx)
- [`androidx.recyclerview:recyclerview`](https://developer.android.com/jetpack/androidx)

## [License](https://bitbucket.org/android-universum/recycler/src/main/LICENSE.md) ##

**Copyright 2020 Universum Studios**

_Licensed under the Apache License, Version 2.0 (the "License");_

You may not use this file except in compliance with the License. You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software distributed under the License
is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
or implied.
     
See the License for the specific language governing permissions and limitations under the License.