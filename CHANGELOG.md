Change-Log
===============
> Regular configuration update: _09.03.2020_

More **detailed changelog** for each respective version may be viewed by pressing on a desired _version's name_.

## Version 1.x ##

### [1.2.3](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 28.04.2020

- Regular **maintenance**.

### [1.2.2](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 17.01.2020

- Resolved [Issue #4](https://bitbucket.org/android-universum/recycler/issues/4).

### [1.2.1](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 07.12.2019

- Regular **dependencies update** (mainly artifacts from **Android Jetpack**).

### [1.2.0](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 13.09.2019

- Added possibility to **change visibility** of item decorations.
- Updated implementation of `ItemSpaceDecoration` to not perform decoration if `shouldDecorate(...)` returns `false`.
- Updated implementation of `ItemDividerDecoration` to support drawing of offset divider in area of start and end offset specified for the decoration.

### [1.1.1](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 05.08.2019

- Fixed wrong implementation of **skipFirst** and **skipLast** in `ItemDividerDecoration` (mainly drawing part).

### [1.1.0](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 18.11.2018

- Regular **dependencies update** (mainly to use new artifacts from **Android Jetpack**).

### [1.0.5](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 02.11.2018

- Small updates and improvements.

### [1.0.4](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 20.07.2018

- Small updates and improvements.

### [1.0.3](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 28.06.2018

- Small updates.

### [1.0.2](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 15.03.2018

- `ItemDividerDecoration` now accepts **offsets** for its divider either via `setDividerOffset(int, int)`
  or via _Xml_ attributes: `recyclerDividerOffsetStart`, `recyclerDividerOffsetEnd`.
- Added ability to specify a `Precondition` for a desired `RecyclerViewItemDecoration` which may be
  useful for more item specific decorating.

### [1.0.1](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 21.05.2017

- `ItemSwipeHelper.OnSwipeListener.onSwipeCanceled(...)` is now not being called if the associated
  `RecyclerView` is **computing layout** a the time when `ItemSwipeHelper.Interactor.clearView(...)`
  is requested.

### [1.0.0](https://bitbucket.org/android-universum/recycler/wiki/version/1.x) ###
> 29.04.2017

- First production release.