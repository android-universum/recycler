/*
 * *************************************************************************************************
 *                                 Copyright 2017 Universum Studios
 * *************************************************************************************************
 *                  Licensed under the Apache License, Version 2.0 (the "License")
 * -------------------------------------------------------------------------------------------------
 * You may not use this file except in compliance with the License. You may obtain a copy of the
 * License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 *
 * See the License for the specific language governing permissions and limitations under the License.
 * *************************************************************************************************
 */
package universum.studios.android.recycler.decoration;

import android.content.Context;
import android.util.AttributeSet;

import org.junit.Test;

import androidx.annotation.AttrRes;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import universum.studios.android.test.AndroidTestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Martin Albedinsky
 */
public final class RecyclerViewItemDecorationTest extends AndroidTestCase {

	@Test public void testInstantiationWithContextAttrsSetDefStyleAttrDefStyleRes() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.ItemDecoration.SkipNone",
				"style",
				context().getPackageName()
		);
		// Act:
		final TestDecoration decoration = new TestDecoration(context(), null, 0, styleResId);
		// Assert:
		assertThat(decoration.skipsFirst(), is(false));
		assertThat(decoration.skipsLast(), is(false));
	}

	@Test public void testInstantiationWithContextAttrsSetDefStyleAttrDefStyleResWithSkipFirst() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.ItemDecoration.SkipFirst",
				"style",
				context().getPackageName()
		);
		// Act:
		final TestDecoration decoration = new TestDecoration(context(), null, 0, styleResId);
		// Assert:
		assertThat(decoration.skipsFirst(), is(true));
		assertThat(decoration.skipsLast(), is(false));
	}

	@Test public void testInstantiationWithContextAttrsSetDefStyleAttrDefStyleResWithSkipLast() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.ItemDecoration.SkipLast",
				"style",
				context().getPackageName()
		);
		// Act:
		final TestDecoration decoration = new TestDecoration(context(), null, 0, styleResId);
		// Assert:
		assertThat(decoration.skipsFirst(), is(false));
		assertThat(decoration.skipsLast(), is(true));
	}

	@Test public void testInstantiationWithContextAttrsSetDefStyleAttrDefStyleResWithSkipBoth() {
		// Arrange:
		final int styleResId = context().getResources().getIdentifier(
				"Test.ItemDecoration.SkipBoth",
				"style",
				context().getPackageName()
		);
		// Act:
		final TestDecoration decoration = new TestDecoration(context(), null, 0, styleResId);
		// Assert:
		assertThat(decoration.skipsFirst(), is(true));
		assertThat(decoration.skipsLast(), is(true));
	}

	private static final class TestDecoration extends RecyclerViewItemDecoration {

		TestDecoration(
				@Nullable final Context context,
				@Nullable final AttributeSet attrs,
				@AttrRes final int defStyleAttr,
				@StyleRes final int defStyleRes
		) { super(context, attrs, defStyleAttr, defStyleRes); }
	}
}